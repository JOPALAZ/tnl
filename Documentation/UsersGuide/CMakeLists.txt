add_subdirectory( Arrays )
add_subdirectory( Vectors )
add_subdirectory( ReductionAndScan )
add_subdirectory( Pointers )
add_subdirectory( NDArrays )
add_subdirectory( Matrices )
add_subdirectory( Meshes )
