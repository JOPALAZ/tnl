# vim: tabstop=4 shiftwidth=4 softtabstop=4

default:
    image: "archlinux/devel-tnl:latest"
    tags:
        - docker
    retry:
        max: 1
        when:
            - api_failure
            - runner_system_failure
            - stuck_or_timeout_failure

stages:
    - lint
    - build
    - build:cuda
    - build:hip_rocm
    - build:host
    - build:doc
    - deploy

# https://docs.gitlab.com/ee/ci/yaml/workflow.html
workflow:
    rules:
        # run a pipeline in the merge request context
        - if: $CI_PIPELINE_SOURCE == "merge_request_event"
        # don't run a branch pipeline on push when there is an open merge request
        - if: $CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
          when: never
        # otherwise run a branch pipeline
        - if: $CI_COMMIT_BRANCH

# default flags for cmake
.default_cmake_flags: &default_cmake_flags
    # architectures
    USE_CUDA: "no"
    USE_HIP: "no"
    # MPI is selectively added to targets, so there is no point to have separate jobs without MPI
    USE_MPI: "yes"
    USE_OPENMP: "yes"
    # build targets
    BUILD_COVERAGE: "no"
    BUILD_DOC: "no"  # only used in the "documentation" job
    BUILD_EXAMPLES: "no"  # only used in the "Ginkgo examples" job
    # toolchain flags defaults
    CMAKE_CXX_FLAGS: ""
    CMAKE_CUDA_FLAGS: ""
    CMAKE_EXE_LINKER_FLAGS: ""
    CMAKE_SHARED_LINKER_FLAGS: ""

# base for GCC builds
.gcc:
    stage: build:host
    variables:
        <<: *default_cmake_flags
        CXX: g++
        CC: gcc

# base for NVCC builds
.nvcc:
    stage: build:cuda
    tags:
        - docker
        - nvidia
    variables:
        <<: *default_cmake_flags
        USE_CUDA: "yes"

# base for NVHPC builds
.nvhpc:
    image: "archlinux/devel-tnl-nvhpc:latest"
    variables:
        <<: *default_cmake_flags
        USE_CUDA: "yes"
        CXX: nvc++
        CC: nvc
        CUDA_HOST_COMPILER: nvc++

# base for Clang builds
.clang:
    stage: build:host
    variables:
        <<: *default_cmake_flags
        CXX: clang++
        CC: clang
        CUDA_HOST_COMPILER: clang++
        # also use LLVM's libc++ instead of GCC's libstdc++ (which is used by default)
        CMAKE_CXX_FLAGS: "-stdlib=libc++"
        # use the LLVM linker and link to libc++
        CMAKE_EXE_LINKER_FLAGS: "-fuse-ld=lld -lc++ -lc++abi"
        CMAKE_SHARED_LINKER_FLAGS: "-fuse-ld=lld -lc++ -lc++abi"

# base for Clang CUDA builds
.clang_cuda:
    stage: build:cuda
    tags:
        - docker
        - nvidia
    extends: .clang
    variables:
        CUDACXX: clang++
        CUDAHOSTCXX: clang++
        CMAKE_CUDA_FLAGS: "-stdlib=libc++"
        USE_CUDA: "yes"
        # use CUDA 11.8 from the Arch Linux-based Docker image
        "CUDA_PATH": "/opt/cuda-11.8"
        "Thrust_DIR": "${CUDA_PATH}/lib64/cmake/thrust/"
        "CUB_DIR": "${CUDA_PATH}/lib64/cmake/cub/"

# base for Intel oneAPI builds
.icpx:
    stage: build:host
    image: "archlinux/devel-tnl-oneapi:latest"
    variables:
        <<: *default_cmake_flags
        CXX: icpx
        CC: icx

# base for HIP/ROCm builds
.hip_rocm:
    stage: build:hip_rocm
    image: "archlinux/devel-tnl-rocm:latest"
    tags:
        - docker
        - hip-rocm
    variables:
        <<: *default_cmake_flags
        USE_HIP: "yes"
        USE_OPENMP: "no"   # TODO: enabling OpenMP leads to false warnings due to unused variable

# template for environment setup before build
.env_setup: &env_setup
    - CUDA_PATH_BACKUP=$CUDA_PATH
    # make sure that $PATH and other essential variables are set correctly
    - source /etc/profile
    # restore $CUDA_PATH if it was set as a variable in the CI
    - if [[ -n ${CUDA_PATH_BACKUP} ]]; then
            export CUDA_PATH=${CUDA_PATH_BACKUP};
      fi
    # all cores including hyperthreading
    #- export NUM_CORES=$(grep "core id" /proc/cpuinfo | wc -l)
    # all pyhsical cores
    - export NUM_CORES=$(grep "core id" /proc/cpuinfo | sort -u | wc -l)
    - export CTEST_OUTPUT_ON_FAILURE=1
    - export CTEST_PARALLEL_LEVEL=4
    - export OMP_NUM_THREADS=4
    # running as root does not matter inside Docker containers
    - export OMPI_ALLOW_RUN_AS_ROOT=1
    - export OMPI_ALLOW_RUN_AS_ROOT_CONFIRM=1
    # configure OpenMPI to run without ssh
    - export OMPI_MCA_plm_rsh_agent=sh

# template for build jobs
.build_template:
    # don't wait for jobs in previous stages to complete before starting this job
    needs: []
    script:
        - *env_setup
        # handle the $MATRIX_TARGET variable from .matrix_template
        - if [[ ${MATRIX_TARGET} == "tests" ]]; then
                BUILD_TARGETS=("non-matrix-tests");
          elif [[ ${MATRIX_TARGET} == "matrix_tests" ]]; then
                BUILD_TARGETS=("matrix-tests");
          elif [[ ${MATRIX_TARGET} == "nontests" ]]; then
                BUILD_TARGETS=("benchmarks" "examples" "tools");
          elif [[ ${BUILD_DOC} == "yes" ]]; then
                BUILD_TARGETS=("documentation");
          elif [[ ${BUILD_EXAMPLES} == "yes" ]]; then
                BUILD_TARGETS=("examples");
          fi
        - cmake -B "./build/$CI_JOB_NAME_SLUG" -S . -G Ninja
                -DCMAKE_BUILD_TYPE=${BUILD_TYPE}
                -DCMAKE_INSTALL_PREFIX="$(pwd)/install_prefix/$CI_JOB_NAME_SLUG"
                -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS}"
                -DCMAKE_CUDA_FLAGS="${CMAKE_CUDA_FLAGS}"
                -DCMAKE_EXE_LINKER_FLAGS="${CMAKE_EXE_LINKER_FLAGS}"
                -DCMAKE_SHARED_LINKER_FLAGS="${CMAKE_SHARED_LINKER_FLAGS}"
                -DTNL_USE_OPENMP=${USE_OPENMP}
                -DTNL_USE_MPI=${USE_MPI}
                -DTNL_USE_CUDA=${USE_CUDA}
                -DTNL_USE_HIP=${USE_HIP}
                -DTNL_BUILD_COVERAGE=${BUILD_COVERAGE}
                -DTNL_USE_CI_FLAGS=yes
        # build the given targets
        - cmake --build "./build/$CI_JOB_NAME_SLUG" --parallel ${NUM_CORES} --target "${BUILD_TARGETS[@]}"
        # we have ctest options set in the preset, but ctest does not allow to override the build directory
        # https://gitlab.kitware.com/cmake/cmake/-/issues/23982
        - "sed 's|\"binaryDir\": \"${sourceDir}/build\",|\"binaryDir\": \"${sourceDir}\",|' CMakePresets.json > \"./build/$CI_JOB_NAME_SLUG/CMakePresets.json\""
        - pushd "./build/$CI_JOB_NAME_SLUG/"
        - if [[ ${MATRIX_TARGET} == "tests" ]]; then
                ctest --preset non-matrix-tests --output-junit "./tests-report.xml";
          elif [[ ${MATRIX_TARGET} == "matrix_tests" ]]; then
                ctest --preset matrix-tests --output-junit "./tests-report.xml";
          fi
        - popd
    rules:
        - changes:
            - src/**/*.{h,hpp,cpp,cu,hip}
            - "**/CMakeLists.txt"
            - cmake/*
            - .gitlab-ci.yml
    interruptible: true

.matrix_template:
    parallel:
        matrix:
            - MATRIX_TARGET:
                - tests
                - matrix_tests
                - nontests
              BUILD_TYPE:
                - Debug
                - Release

# template for collecting code coverage
.coverage_template:
    # NOTE: gcovr is very slow, maybe it could be solved with fastcov:
    # https://github.com/gcovr/gcovr/issues/289
    # https://github.com/RPGillespie6/fastcov
    # NOTE: when enabling, the BUILD_COVERAGE env var must be switched to yes
    #after_script:
    #    - mkdir coverage_html
    #    - if [[ ${CXX} == "clang++" ]]; then
    #            GCOV_COMMAND="llvm-cov gcov";
    #      else
    #            GCOV_COMMAND="gcov";
    #      fi
    #    - gcovr --print-summary --html-details coverage_html/coverage.html --xml coverage.xml --xml-pretty --gcov-executable "${GCOV_COMMAND}" --exclude-unreachable-branches --root "${CI_PROJECT_DIR}" --filter "${CI_PROJECT_DIR}/src/TNL/"
    coverage: /^\s*lines:\s*\d+.\d+\%/
    artifacts:
        name: ${CI_JOB_NAME_SLUG}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}
        expire_in: 7 days
        reports:
            coverage_report:
                coverage_format: cobertura
                path: coverage.xml
            junit: "build/$CI_JOB_NAME_SLUG/tests-report.xml"
        paths:
            - coverage_html/

# template for registering tests-report.xml as an artifact
.tests_report_template:
    artifacts:
        name: ${CI_JOB_NAME}-${CI_COMMIT_REF_NAME}-${CI_COMMIT_SHA}
        expire_in: 7 days
        reports:
            junit: "build/$CI_JOB_NAME_SLUG/tests-report.xml"

# Dummy build job to ensure that a pipeline is created for a merge request, even
# when there were no changes.
dummy build job:
    stage: build
    script: echo "dummy"
    only:
        - merge_requests
    except:
        changes:
            # .build_template
            - src/**/*.{h,hpp,cpp,cu,hip}
            - "**/CMakeLists.txt"
            - cmake/*
            - .gitlab-ci.yml
            # build documentation
            - Documentation/**/*





nvcc:
    extends:
        - .build_template
        - .matrix_template
        - .nvcc

# FIXME: builds with NVHPC 23.7 don't pass due to toolchain bugs
#nvhpc:
#    stage: build:cuda
#    tags:
#        - docker
#        - nvidia
#    extends:
#        - .build_template
#        - .matrix_template
#        - .nvhpc

clang_cuda:
    extends:
        - .build_template
        - .matrix_template
        - .clang_cuda


hip_rocm:
    extends:
        - .build_template
        - .matrix_template
        - .hip_rocm


gcc:
    extends:
        - .build_template
        - .matrix_template
        - .gcc

clang:
    extends:
        - .build_template
        - .matrix_template
        - .clang

icpx:
    extends:
        - .build_template
        - .matrix_template
        - .icpx





Ginkgo examples:
    extends: .build_template
    stage: build:cuda
    tags:
        - docker
        - nvidia
    variables:
        <<: *default_cmake_flags
        USE_CUDA: "yes"
        BUILD_TYPE: Debug
        # build the TNL examples
        BUILD_EXAMPLES: "yes"
    before_script:
        - *env_setup
        # clone and build Ginkgo
        - git clone https://github.com/ginkgo-project/ginkgo.git
        - cmake -B "./build_ginkgo/$CI_JOB_NAME_SLUG" -S "ginkgo" -G Ninja
                -DCMAKE_BUILD_TYPE=${BUILD_TYPE}
                -DGINKGO_BUILD_TESTS="no"
                -DGINKGO_BUILD_EXAMPLES="no"
                -DGINKGO_BUILD_BENCHMARKS="no"
        # "install" implies the "all" target
        - cmake --build "./build_ginkgo/$CI_JOB_NAME_SLUG" --parallel ${NUM_CORES} --target install





documentation:
    extends: .build_template
    stage: build:cuda
    tags:
        - docker
        - nvidia
    variables:
        <<: *default_cmake_flags
        USE_OPENMP: "yes"
        USE_CUDA: "yes"
        USE_MPI: "yes"
        BUILD_TYPE: Debug
        # build output snippets for documentation
        BUILD_DOC: "yes"
    rules:
        - changes:
            - Documentation/**/*
            - src/TNL/**/*.{h,hpp}
            - .gitlab-ci.yml
    # store the built documentation for deployment
    after_script:
        - mv "./build/$CI_JOB_NAME_SLUG/Documentation/html" "./html"
    artifacts:
        paths:
            - ./html/
        expire_in: 7 days

pages:
    stage: deploy
    rules:
        - if: $CI_PIPELINE_SOURCE == "schedule" || $CI_PIPELINE_SOURCE == "trigger" || $CI_COMMIT_BRANCH == "main"
          changes:
            - Documentation/**/*
            - src/TNL/**/*.{h,hpp}
            - .gitlab-ci.yml
    # use "dependencies" instead of "needs" to deploy only when the entire pipeline succeeds
    dependencies:
        - documentation
    image: "archlinux:latest"
    script:
        - mv ./html/ public/
    artifacts:
        paths:
            - public

copyright-headers:
    stage: lint
    rules:
        - changes:
            - src/**/*.{h,hpp,cpp,cu,hip}
            - scripts/updage-copyright-headers.py
    script:
        - ./scripts/update-copyright-headers.py
                --dir src/
                --exclude
                    'src/TNL/3rdparty/*'
                    'src/UnitTests/*'
                    'src/Benchmarks/Sorting/ReferenceAlgorithms/*'
                    'src/Examples/Hypre/*'
        - git --no-pager diff --color=always --exit-code
    interruptible: true

clang-format src/TNL:
    stage: lint
    rules:
        - changes:
            - src/TNL/**/*.{h,hpp,cpp,cu,hip}
    script:
        - ./scripts/run-clang-format.py
                --color always
                --style file
                --exclude "src/TNL/3rdparty/*"
                --recursive
                src/TNL
    interruptible: true

clang-format src/Benchmarks:
    stage: lint
    rules:
        - changes:
            - src/Benchmarks/**/*.{h,hpp,cpp,cu,hip}
    script:
        - ./scripts/run-clang-format.py
                --color always
                --style file
                --recursive
                src/Benchmarks
    interruptible: true
    # TODO: remove to enforce formatting
    allow_failure: true

clang-format src/Examples:
    stage: lint
    rules:
        - changes:
            - src/Examples/**/*.{h,hpp,cpp,cu,hip}
    script:
        - ./scripts/run-clang-format.py
                --color always
                --style file
                --recursive
                src/Examples
    interruptible: true
    # TODO: remove to enforce formatting
    allow_failure: true

clang-format src/Tools:
    stage: lint
    rules:
        - changes:
            - src/Tools/**/*.{h,hpp,cpp,cu,hip}
    script:
        - ./scripts/run-clang-format.py
                --color always
                --style file
                --recursive
                src/Tools
    interruptible: true

clang-format src/UnitTests:
    stage: lint
    rules:
        - changes:
            - src/UnitTests/**/*.{h,hpp,cpp,cu,hip}
    script:
        - ./scripts/run-clang-format.py
                --color always
                --style file
                --recursive
                src/UnitTests
    interruptible: true

clang-format Documentation:
    stage: lint
    rules:
        - changes:
            - Documentation/**/*.{h,hpp,cpp,cu,hip}
    script:
        - ./scripts/run-clang-format.py
                --color always
                --style file
                --recursive
                Documentation
    interruptible: true
    # TODO: remove to enforce formatting
    allow_failure: true

clang-tidy:
    stage: lint
    rules:
        - changes:
            - Documentation/**/*.{h,hpp,cpp,cu,hip}
            - src/**/*.{h,hpp,cpp,cu,hip}
            - "**/CMakeLists.txt"
            - cmake/*
            - .gitlab-ci.yml
    variables:
        CXX: clang++
        CC: clang
    script:
        # configure only to generate compile_commands.json
        - cmake -B "./build/$CI_JOB_NAME_SLUG" -S . -G Ninja
                -DCMAKE_BUILD_TYPE=Debug
                -DTNL_USE_OPENMP=yes
                -DTNL_USE_MPI=yes
                -DTNL_USE_CUDA=no
                -DTNL_USE_CI_FLAGS=yes
        # cmake creates compile_commands.json only on the second run, WTF!?
        - cmake -B "./build/$CI_JOB_NAME_SLUG" -S . -G Ninja
        - ls -lah "./build/$CI_JOB_NAME_SLUG"
        # run-clang-tidy is weird compared to run-clang-format.py:
        # - clang-tidy is not executed on header files, but on source files
        # - the positional arguments are regexes filtering sources from the compile_commands.json
        # - the -header-filter option (or HeaderFilterRegex in the config) allows to filter header files
        # - "build/.*" must be excluded on run-clang-tidy command line as it
        #   includes targets generated by CMake's FetchContent module
        - run-clang-tidy -p "./build/$CI_JOB_NAME_SLUG" "$PWD/Documentation/.*" "$PWD/src/.*"
    interruptible: true
    # TODO: remove to enforce
    allow_failure: true
